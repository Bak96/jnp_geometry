g++ -c -Wall -O2 -std=c++14 ../geometry.cc -o geometry.o
g++ -c -Wall -O2 -std=c++14 test1.cc -o test1.o
g++ geometry.o test1.o -o test1

g++ -c -Wall -O2 -std=c++14 test2.cc -o test2.o
g++ geometry.o test2.o -o test2

g++ -c -Wall -O2 -std=c++14 test3.cc -o test3.o
g++ geometry.o test3.o -o test3

g++ -c -Wall -O2 -std=c++14 test4.cc -o test4.o
g++ geometry.o test4.o -o test4


rm *.o