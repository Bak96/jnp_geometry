#include <cassert>
#include <iostream>
#include "../geometry.h"

int main() {
	auto v = Vector(10, 10);
    auto rec1 = Rectangle(100, 200);
    auto rec2 = Rectangle(200, 200);
    const Rectangle rec3 = Rectangle(100, 200);
    const Rectangle rec4 = Rectangle(200, 200);
    assert(rec3 == rec1);
    auto foo = Rectangles({rec1, rec2});
    auto bar = Rectangles({rec3, rec4});
    assert(foo[0].width() == 100 && foo[0].height() == 200);

    // PorĂłwnywanie i przesuwanie o vector
    assert(foo == bar);
    foo += v;
    assert(foo != bar);
    bar += v;
    assert(foo == bar);
    assert(foo[0].pos() == Position(10, 10));

    // Pobieranie elementow
    rec1 += v;
    assert(foo[0].pos() == Position(10, 10));

    // Bezargumentowy konstruktor
    auto rc = Rectangles();
    assert(rc.size() == 0);

    // Split
    foo.split_vertically(1, 110);
    auto res1 = Rectangle(110, 200, Position(10, 10));
    auto res2 = Rectangle(90, 200, Position(120, 10));
    assert(foo[1] == res1 && foo[2] == res2);
    foo.split_horizontally(0, 20);
    res1 = Rectangle(100, 20, Position(10, 10));
    res2 = Rectangle(100, 180, Position(10, 30));
    assert(foo[0] == res1 && foo[1] == res2);
}
