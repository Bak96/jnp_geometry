#include <cassert>
#include <iostream>
#include "../geometry.h"


int main() {

	std::cout << "Creating Position\n";

	Position pos;
	Vector vec(10,20);

	assert(pos.x() == 0);
	assert(pos.y() == 0);

	assert(Position::origin().x() == 0);
	assert(Position::origin().y() == 0);

	std::cout << "Adding vector to Position\n";

	pos = pos + vec;

	assert(pos.x() == 10);
	assert(pos.y() == 20);
	
	std::cout << "Using Position::origin\n";

	pos = Position::origin();
	
	std::cout << "Adding vector to that origin\n";

	pos += vec;

	//assert(pos.x() == 0);
	//assert(pos.y() == 0);
	std::cout << "Using origin again\n";

	pos = Position::origin();

	assert(pos.x() == 0);
	assert(pos.y() == 0);

	return 0;
}
