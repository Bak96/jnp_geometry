#include <cassert>
#include <iostream>
#include "../geometry.h"
 
int main() {
    Position a(2, 3);
    Position b(1, 1);
    assert(b.x() == 1 && b.y() == 1);
    Position c = a.reflection();
    assert(a == c.reflection());

    Position d = Position::origin();
    Position e = Position::origin();
    assert(d == e);
    //assert(&d == &e); //POWINNY BYC TE SAME ADRESY
    Vector vec1 = Vector(3, 4);
    assert((a + vec1).x() == 5 && (a + vec1).y() == 7);

    Vector vec2 = Vector(1, 2);
    assert((vec1 + vec2).x() == 4 && (vec1 + vec2).y() == 6);
    Vector vec3 = Vector(2, 4);

    Rectangle r1(4, 5);
    Rectangle r2(4, 5, d);
    assert(r1 == r2);
    Rectangle r3 = r1 + vec2;
    assert(r3 + vec2 == r1 + vec3);

    Rectangle r4(4, 5, d);
    assert(r4.pos() == r2.pos());

    Position g(3, 3);
    g+= vec2;
    assert(g.x() == 4 && g.y() == 5);
}
