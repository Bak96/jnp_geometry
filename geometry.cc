#include "geometry.h"

/*
 * #############################################################################
 * Position
 * #############################################################################
 */

Position::Position() {
	_x = 0;
	_y = 0;
}

Position::Position(const int& cx, const int& cy) { 
	_x = cx;
	_y = cy;
}


int Position::x() const {
	return _x;
}

int Position::y() const {
	return _y;
}

Position Position::reflection() const {
	return Position(y(), x());
}

bool Position::operator==(const Position& other) const {
	return x() == other.x() && y() == other.y();
}

bool Position::operator!=(const Position& other) const {
	return !(*this == other);
}

Position & Position::operator+=(const Vector& vec) { 
	_x += vec.x();
	_y += vec.y();
	return *this;
}

const Position & Position::origin() { //Might need better
	static const Position _origin(0,0);
	return _origin;
}

/*
 * #############################################################################
 * Vector
 * #############################################################################
 */

Vector::Vector() {
	_x = 0;
	_y = 0;
}

Vector::Vector(const int &cx, const int &cy) {
	_x = cx;
	_y = cy;
}

int Vector::x() const {
	return _x;
}

int Vector::y() const {
	return _y;
}

Vector Vector::reflection() const {
	return Vector(y(), x());
}

bool Vector::operator==(const Vector& other) const {
	return x() == other.x() && y() == other.y();
}

bool Vector::operator!=(const Vector& other) const {
	return !(*this == other);
}

Vector & Vector::operator+=(const Vector& vec) {
	_x += vec.x();
	_y += vec.y();
	return *this;
}

/*
 * #############################################################################
 * Rectangle
 * #############################################################################
 */

Rectangle::Rectangle(const int &width, const int &height):
	_width(width), _height(height), _pos(Position(0, 0)) {
	
	assert(width > 0 && height > 0);
}
Rectangle::Rectangle(const int &width, const int &height, const Position &pos):
	_width(width), _height(height), _pos(pos) {
	
	assert(width > 0 && height > 0);
}

int Rectangle::width() const {
	return _width;
}

int Rectangle::height() const {
	return _height;
}

Position Rectangle::pos() const {
	return _pos;
}

int Rectangle::area() const {
	return _width * _height;
}

Rectangle Rectangle::reflection() const {
	return Rectangle(_height, _width, _pos.reflection());
}

Rectangle & Rectangle::operator+=(const Vector& vec) {
	_pos += vec;
	return *this;
}

bool Rectangle::operator==(const Rectangle &other) const{
	return height() == other.height() && width() == other.width() && pos() == other.pos();
}

bool Rectangle::operator!=(const Rectangle &other) const{
	return !(*this == other);
}

std::pair<Rectangle, Rectangle> Rectangle::split_horizontally(int place) {
	Position p1 = _pos;
	Position p2 = Position(_pos.x(), _pos.y() + place);
	int h1 = place;
	int h2 = height() - place;
	
	return std::make_pair(Rectangle(width(), h1, p1), Rectangle(width(), h2, p2));
}

std::pair<Rectangle, Rectangle> Rectangle::split_vertically(int place) {
	Position p1 = _pos;
	Position p2 = Position(_pos.x() + place, _pos.y());
	int w1 = place;
	int w2 = width() - place;
	
	return std::make_pair(Rectangle(w1, height(), p1), Rectangle(w2, height(), p2));
}

/*
 * #############################################################################
 * Rectangles
 * #############################################################################
 */

Rectangles::Rectangles(){}
Rectangles::Rectangles(const std::vector<Rectangle> &recs): _recs(recs) {}
Rectangles::Rectangles(std::vector<Rectangle> &&recs): _recs(std::move(recs)) {}

int Rectangles::size() const {
	return _recs.size();
}

void Rectangles::split_horizontally(size_t idx, int place) {
	assert(idx >= 0 && idx < _recs.size());
	
	auto it = _recs.begin() + idx;
	std::pair<Rectangle, Rectangle> split = it->split_horizontally(place);
	
	*it = split.first;
	it++;
	_recs.insert(it, split.second);
}

void Rectangles::split_vertically(size_t idx, int place) {
	assert(idx >= 0 && idx < _recs.size());
	
	auto it = _recs.begin() + idx;
	std::pair<Rectangle, Rectangle> split = it->split_vertically(place);
	
	*it = split.first;
	it++;
	_recs.insert(it, split.second);
}
	
Rectangle & Rectangles::operator[](size_t idx) {
	assert(idx >= 0 && idx < _recs.size());
	return _recs[idx];
}

bool Rectangles::operator==(const Rectangles &other) const {
	return _recs == other._recs;
}

bool Rectangles::operator!=(const Rectangles &other) const {
	return !(*this == other);
}

Rectangles & Rectangles::operator+=(const Vector &vec) {
	for (Rectangle &r : _recs) {
		r += vec;
	}
	
	return *this;
}


/*
 * #############################################################################
 * Additional functions and operators
 * #############################################################################
 */

Rectangle merge_horizontally(Rectangle rect1, Rectangle rect2) {
	assert(rect1.pos().x() == rect2.pos().x());
	assert(rect1.width() == rect2.width());
	assert(rect1.pos().y() + rect1.height() == rect2.pos().y());
	return Rectangle(rect1.width(), rect1.height() + rect2.height(), rect1.pos());

}

Rectangle merge_vertically(Rectangle rect1, Rectangle rect2) {
	assert(rect1.pos().y() == rect2.pos().y());
	assert(rect1.height() == rect2.height());
	assert(rect1.pos().x() + rect1.width() == rect2.pos().x());
	return Rectangle(rect1.width() + rect2.width(), rect1.height(), rect1.pos());

}

Position operator+(const Position& pos, const Vector& vec) {
	return Position(pos) += vec;
}

Position operator+(Position&& pos, const Vector& vec) {
	return pos += vec;
}

Position operator+(const Vector& vec, const Position& pos) {
	return Position(pos) += vec;
}

Position operator+(const Vector& vec, Position&& pos) {
	return pos += vec;
}

Vector operator+(const Vector& v1, const Vector& v2) {
	return Vector(v1) += v2;
}

Vector operator+(Vector&& v1, const Vector& v2) {
	return v1 += v2;
}

Vector operator+(const Vector& v1, Vector&& v2) {
	return v2 += v1;
}

Rectangle operator+(const Rectangle& rect, const Vector& vec) {
	return Rectangle(rect) += vec;
}

Rectangle operator+(Rectangle&& rect, const Vector& vec) {
	return rect += vec;
}

Rectangle operator+(const Vector& vec, const Rectangle& rect) {
	return Rectangle(rect) += vec;
}

Rectangle operator+(const Vector& vec, Rectangle&& rect) {
	return rect += vec;
}

Rectangles operator+(const Rectangles& recs, const Vector& vec) {
	return Rectangles(recs) += vec;
}

Rectangles operator+(Rectangles&& recs, const Vector& vec) {
	return recs += vec;
}

Rectangles operator+(const Vector& vec, const Rectangles& recs) {
	return Rectangles(recs) += vec;
}

Rectangles operator+(const Vector& vec, Rectangles&& recs) {
	return recs += vec;
}
