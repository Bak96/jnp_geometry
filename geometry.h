#ifndef GEOMETRY_H
#define GEOMETRY_H
#include <utility>
#include <vector>
#include <cstdlib>
#include <cassert>

class Position;
class Vector;
class Rectangle;
class Rectangles;

class Position {
	private:		
		int _x;
		int _y;
	public:
		Position();
		Position(const int& cx, const int& cy);

		int x() const;
		int y() const;
		Position reflection() const;
		static const Position & origin();
		
		bool operator==(const Position& other) const;
		bool operator!=(const Position& other) const;
		Position & operator+=(const Vector& vec);
};

class Vector {
	private:		
		int _x;
		int _y;
	public:
		Vector();
		Vector(const int &cx, const int &cy);
		
		int x() const;
		int y() const;
		Vector reflection() const;
		
		bool operator==(const Vector& other) const;
		bool operator!=(const Vector& other) const;
		
		Vector & operator+=(const Vector& vec);
};

class Rectangle {
	private:
		int _width;
		int _height;
		Position _pos;
	public:
		Rectangle(const int &width, const int &height);
		Rectangle(const int &width, const int &height, const Position &pos);
		
		int width() const;
		int height() const;
		Position pos() const;
		int area() const;
		std::pair<Rectangle, Rectangle> split_horizontally(int place);
		std::pair<Rectangle, Rectangle> split_vertically(int place);
		Rectangle reflection() const;
		
		Rectangle & operator+=(const Vector& vec);
		bool operator==(const Rectangle &other) const;
		bool operator!=(const Rectangle &other) const;
};

class Rectangles {
	private:
		std::vector<Rectangle> _recs;
	public:
		Rectangles();
		Rectangles(const std::vector<Rectangle> &recs);
		Rectangles(std::vector<Rectangle> &&recs);
		
		int size() const;
		void split_horizontally(size_t idx, int place);
		void split_vertically(size_t idx, int place);
		
		Rectangle & operator[](size_t idx);
		bool operator==(const Rectangles &other) const;
		bool operator!=(const Rectangles &other) const;
		Rectangles & operator+=(const Vector &vec);	
};

Rectangle merge_vertically(Rectangle rect1, Rectangle rect2);
Rectangle merge_horizontally(Rectangle rect1, Rectangle rect2);

Position operator+(const Position& pos, const Vector& vec);
Position operator+(Position&& pos, const Vector& vec);
Position operator+(const Vector& vec, const Position& pos);
Position operator+(const Vector& vec, Position&& pos);
Vector operator+(const Vector& v1, const Vector& v2);
Vector operator+(Vector&& v1, const Vector& v2);
Vector operator+(const Vector& v1, Vector&& v2);
Rectangle operator+(const Rectangle& rect, const Vector& vec);
Rectangle operator+(Rectangle&& rect, const Vector& vec);
Rectangle operator+(const Vector& vec, const Rectangle& rect);
Rectangle operator+(const Vector& vec, Rectangle&& rect);
Rectangles operator+(const Rectangles& recs, const Vector& vec);
Rectangles operator+(Rectangles&& recs, const Vector& vec);
Rectangles operator+(const Vector& vec, const Rectangles& recs);
Rectangles operator+(const Vector& vec, Rectangles&& recs);
#endif
